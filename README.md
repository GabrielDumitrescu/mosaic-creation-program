# Mosaic-creation-program
First homework from the computer vision class
  
How to run:  
-create a "data" folder to add extracted "colectie" and "imaginiTest" inside  
-optional: download, extract and add CIFAR-10(https://www.cs.toronto.edu/~kriz/cifar.html) into your "data" folder for additional mosaic pieces  
-set parameters in params.py  
-run ruleazaProiect.py  
